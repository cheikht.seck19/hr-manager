FROM openjdk:8
ADD target/sb-test-0.0.1.jar sb-test-0.0.1.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "sb-test-0.0.1.jar"]