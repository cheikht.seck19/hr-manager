# todo:

1. As HR Manager
* I want to create or delete departments
* I want to promote employees as department Manager
* I want to create new employee
  * username must be unique and unmodifiable
  * password must be set

2. As a department manager
* I want enrol unassigned employee in my department 
* I want to remove employee from my department
* I want a daily employees time report
  * the report must be placed in a configurable directory.
  * One directory per department
  * A report is a file named "report_{yyyy-mm-dd}.csv"
  * for each of my employee a record containing the project name, the reported time and when the time has been reported

3. As an employee
* I want to update my personal informations (password, firstname, lastname)
* I want to add my time report in my timesheet

# considerations
* A initial list of employee and their enrollment must be loaded at startup
* The application should be able to migrate database at startup
* This application must be secure
* This application should be easily monitored
* provide all information to build the application and package it into a docker image
* provide all information to run and configure the application
* (optional) integration with a SSO provider (eg: keycloak)