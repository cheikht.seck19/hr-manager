package be.app.sb.service;

import be.app.sb.dto.EmployeeDto;
import be.app.sb.model.Role;
import be.app.sb.model.RoleName;
import be.app.sb.model.User;
import be.app.sb.repositories.RoleRepository;
import be.app.sb.repositories.UserRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static be.app.sb.db.tables.Employee.EMPLOYEE;

@Service
public class BootstrapService {

    @Autowired
    private DSLContext dsl;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    PasswordEncoder encoder;

    public void populateDB() {
        List<EmployeeDto> employees = this.dsl.select(EMPLOYEE.ID, EMPLOYEE.USER_NAME)
                .from(EMPLOYEE)
                .fetchInto(EmployeeDto.class);

        List<Integer> employeesWithUserAccount =
                userRepository.findAllByEmployeeIsNotNull().stream()
                .map(user -> user.getEmployee()).collect(Collectors.toList());

        List<EmployeeDto> employeesWithoutAccount = employees.stream()
                .filter(employee -> !employeesWithUserAccount.contains(employee.getId()))
                .collect(Collectors.toList());

        employeesWithoutAccount.forEach(employee -> createUserAccount(employee.getUserName(), employee.getUserName(), employee.getId()));
    }

    public void createUserAccount(String userName, String password, Integer id) {
        User user = new User(userName, encoder.encode(password));
        user.setEmployee(id);
        Set<Role> roles = new HashSet<>();
        Role emplRole = roleRepository.findByName(RoleName.ROLE_EMPLOYEE)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
        roles.add(emplRole);
        user.setRoles(roles);
        userRepository.save(user);
    }
}
