package be.app.sb.service;

import be.app.sb.dto.EmployeeDto;
import be.app.sb.dto.TimesheetDto;
import be.app.sb.model.Project;
import be.app.sb.model.Timesheet;
import be.app.sb.model.User;
import be.app.sb.repositories.TimesheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class TimesheetService {

    @Autowired
    private TimesheetRepository timesheetRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private ProjectService projectService;

    public List<Timesheet> findAllDaily() {
        return timesheetRepository.findAllByDateReported(LocalDate.now());
    }

    public Boolean reportTime(EmployeeDto employeeDto, TimesheetDto timesheetDto) {
        User user = userDetailsService.getUserByUsername(employeeDto.getUserName());
        Project project = projectService.get(timesheetDto.getProjectId());
        Timesheet timesheet = new Timesheet(project, user, timesheetDto);
        return Objects.nonNull(timesheetRepository.save(timesheet));
    }

}
