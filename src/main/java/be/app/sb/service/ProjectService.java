package be.app.sb.service;

import be.app.sb.dto.ProjectDto;
import be.app.sb.model.Project;
import be.app.sb.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public List<ProjectDto> findAll() {
        return this.projectRepository.findAll().stream().map(project -> new ProjectDto(project)).collect(Collectors.toList());
    }

    public Project get(Long id) {
        return this.projectRepository.findById(id).orElse(null);
    }

    public ProjectDto findById(Long id) {
        ProjectDto projectDto = null;
        Project project = get(id);
        if (Objects.nonNull(project)) {
            projectDto = new ProjectDto(project);
        }
        return projectDto;
    }

    public ProjectDto save(String name) {
        Project project = new Project(name);
        this.projectRepository.save(project);
        return new ProjectDto(project);
    }

    public ProjectDto update(Long id, String name) {
        Optional<Project> project = this.projectRepository.findById(id);
        ProjectDto projectDto = null;
        if (project.isPresent()) {
            project.get().setName(name);
            projectRepository.save(project.get());
            projectDto = new ProjectDto(project.get());
        }
        return projectDto;
    }

    public Boolean delete(Long id) {
        Optional<Project> project = projectRepository.findById(id);
        if (project.isPresent()) {
            this.projectRepository.delete(project.get());
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
