package be.app.sb.service;

import be.app.sb.config.JwtProvider;
import be.app.sb.db.tables.pojos.Employee;
import be.app.sb.db.tables.pojos.Department;
import be.app.sb.dto.EmployeeDto;
import be.app.sb.exceptions.CustomFileNotFoundException;
import be.app.sb.model.Timesheet;
import be.app.sb.model.User;
import be.app.sb.repositories.UserRepository;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static be.app.sb.db.tables.Department.DEPARTMENT;
import static be.app.sb.db.tables.Employee.EMPLOYEE;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UtilsService {

    @Autowired
    private JwtProvider tokenProvider;

    @Autowired
    private DSLContext dsl;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TimesheetService timesheetService;

    @Value("${reports.location.path}")
    private String reportsRootPath;

    public EmployeeDto getUserDto(HttpServletRequest request) {
        String jwt;
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            jwt = authHeader.replace("Bearer ", "");
            if (jwt != null && tokenProvider.validateJwtToken(jwt)) {
                String username = tokenProvider.getUserNameFromJwtToken(jwt);
                EmployeeDto employeeDto = dsl.select(DSL.asterisk()).from(EMPLOYEE)
                        .where(EMPLOYEE.USER_NAME.eq(username))
                        .fetchOneInto(EmployeeDto.class);
                User user = userRepository.findByUsername(username).orElseThrow(() -> new RuntimeException("Compte utilisateur inéxistant"));
                employeeDto.setUserId(user.getId());
                employeeDto.setRole(user.getRoles().stream()
                        .map(role -> role.getName().toString()).collect(Collectors.toList()));
                return employeeDto;
            }
        }
        return null;
    }

    @Scheduled(cron = "${scheduling.job.cron}")
    public void generateDailyReports() {
        List<Timesheet> dailyReportedTimesheet = timesheetService.findAllDaily();
        System.out.println("dailyReportedTimesheet: " + dailyReportedTimesheet.size());
        dsl.selectFrom(DEPARTMENT)
                .fetchInto(Department.class).forEach(department -> generateDailyReport(dailyReportedTimesheet, department));

    }

    private void generateDailyReport(List<Timesheet> dailyReportedTimesheet, Department department) {
        String dir = reportsRootPath + File.separator + department.getNo();
        File repertoire = new File(dir);
        if (!repertoire.exists()) {
            repertoire.mkdirs();
        }
//        A report is a file named "report_{yyyy-mm-dd}.csv"
//        for each of my employee a record containing the project name, the reported time and when the time has been reported
        List<Employee> employees = dsl.selectFrom(EMPLOYEE).where(EMPLOYEE.DEPARTMENT_ID.eq(department.getId()))
                .fetchInto(Employee.class);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        List<Long> users = userRepository.findAllByEmployeeIn(employees.stream().map(Employee::getId).collect(Collectors.toList()))
                .stream().map(User::getId).collect(Collectors.toList());
        String filename = "report_" + formatter.format(new Date()) + ".csv";
        File fichier = new File(dir + File.separator + filename);
        CsvWriterSettings settings = new CsvWriterSettings();
        settings.setQuoteAllFields(true);
        settings.getFormat().setQuote('"');
        settings.getFormat().setLineSeparator("\n");
        settings.getFormat().setDelimiter(",");
        CsvWriter writer = new CsvWriter(fichier, settings);
        List<Object[]> rows = new ArrayList<>();
        dailyReportedTimesheet.stream().filter(report -> users.contains(report.getUser().getId())).forEach(report -> {
            System.out.println("report: " + report.getUser().getEmployee());
            Employee emp = employees.stream().filter(employee -> employee.getUserName().equals(report.getUser().getUsername()))
                    .findFirst().get();
            Duration duree = Duration.between(report.getStartedAt(), report.getFinishedAt());
            rows.add(new Object[]{
                    emp.getFirstName() + " " + emp.getLastName(),
                    report.getProject().getName(),
                    format(duree),
                    report.getDateReported().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))});
        });
        writer.writeRowsAndClose(rows);

    }

    private String format(Duration duree) {
        return String.format("%02d:%02d", duree.toHours(), (duree.toMinutes() - duree.toHours() * 60));
    }

    public Resource getReport(String dept, LocalDate date) {
        String fileName = "report_" + date + ".csv";
        try {
            String dir = reportsRootPath + File.separator + dept;
            File repertoire = new File(dir);
            if (!repertoire.exists()) {
                throw new RuntimeException("Aucun rapport n'a été généré pour le moment");
            }
            Path filePath = Paths.get(dir).toAbsolutePath().normalize().resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new CustomFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new CustomFileNotFoundException("File not found " + fileName, ex);
        }
    }
}
