package be.app.sb.model;

public enum RoleName {
    ROLE_HRMANAGER,
    ROLE_DEPTMANAGER,
    ROLE_EMPLOYEE
}
