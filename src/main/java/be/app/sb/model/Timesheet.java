package be.app.sb.model;

import be.app.sb.dto.TimesheetDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Timesheet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="project_id", referencedColumnName="id")
    private Project project;

    private LocalDate dateReported;

    private LocalDateTime startedAt;

    private LocalDateTime finishedAt;

    @OneToOne
    private User user;

    public Timesheet(@NotNull Project project, @NotNull User employee, TimesheetDto timesheetDto) {
        this.project = project;
        this.user = employee;
        this.dateReported = LocalDate.now();
        this.startedAt = timesheetDto.getStartedAt();
        this.finishedAt = timesheetDto.getFinishedAt();
    }
}
