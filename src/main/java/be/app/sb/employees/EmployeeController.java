package be.app.sb.employees;

import be.app.sb.db.tables.daos.EmployeeDao;
import be.app.sb.db.tables.pojos.Employee;
import be.app.sb.dto.EmployeeDto;
import be.app.sb.dto.PersonalInfos;
import be.app.sb.dto.TimesheetDto;
import be.app.sb.model.Role;
import be.app.sb.model.RoleName;
import be.app.sb.model.User;
import be.app.sb.repositories.RoleRepository;
import be.app.sb.repositories.UserRepository;
import be.app.sb.service.BootstrapService;
import be.app.sb.service.TimesheetService;
import be.app.sb.service.UtilsService;
import io.jsonwebtoken.lang.Strings;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static be.app.sb.db.tables.Employee.EMPLOYEE;
import static be.app.sb.db.tables.Department.DEPARTMENT;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeDao employeeDao;

    private final DSLContext dsl;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final BootstrapService bootstrapService;

    private final UtilsService utilsService;

    private final TimesheetService timesheetService;

    private final PasswordEncoder encoder;

    /**
     * Constructeur
     * @param dsl
     * @param jooqConfiguration
     * @param userRepository
     * @param roleRepository
     * @param bootstrapService
     * @param utilsService
     * @param timesheetService
     * @param encoder
     */
    public EmployeeController(DSLContext dsl, Configuration jooqConfiguration,
                              UserRepository userRepository, RoleRepository roleRepository,
                              BootstrapService bootstrapService, UtilsService utilsService,
                              TimesheetService timesheetService, PasswordEncoder encoder) {
        Settings settings = new Settings()
                .withRenderNameStyle(RenderNameStyle.AS_IS);
        jooqConfiguration.set(settings);
        this.employeeDao = new EmployeeDao(jooqConfiguration);
        this.dsl = dsl;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bootstrapService = bootstrapService;
        this.utilsService = utilsService;
        this.timesheetService = timesheetService;
        this.encoder = encoder;
    }

    /**
     * retourne la liste de tous les employés
     * @return
     */
    @GetMapping("/")
    @PreAuthorize("hasRole('HRMANAGER')")
    public List<Employee> employees() {
        return this.employeeDao.findAll();
    }

    /**
     * supprime un employé en se basant sur son id
     * @param id: l'id de l'employé à supprimer
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('HRMANAGER')")
    public void delete(@RequestParam("id") int id) {
        this.employeeDao.deleteById(id);
    }

    /**
     * ajoute un nouvel employé
     * @param newEmployee: les informations de l'employé
     * @return l'employé créé avec son id
     */
    @PostMapping("/")
    @PreAuthorize("hasRole('HRMANAGER')")
    public Employee newEmployee(@RequestBody @Valid EmployeeDto newEmployee) {
        this.employeeDao.insert(newEmployee);
        bootstrapService.createUserAccount(
                newEmployee.getUserName(),
                newEmployee.getPassword(),
                newEmployee.getId());
        return newEmployee;
    }

    /**
     * promeut un employé en tant que manager de département
     * @param username: le login de l'employé à promouvoir
     * @param departmentId: le département cible
     * @return le résultat de l'insertion
     */
    @PostMapping("/promote/{username}")
    @PreAuthorize("hasRole('HRMANAGER')")
    public int promoteEmployee(@PathVariable("username") String username,
                               @RequestBody Integer departmentId) {

        Integer employeeId = this.dsl
                .select(EMPLOYEE.ID)
                .from(EMPLOYEE)
                .where(EMPLOYEE.USER_NAME.equal(username))
                .fetch().getValue(0, EMPLOYEE.ID);

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new RuntimeException("User doesn't exist"));
        Role deptRole = roleRepository.findByName(RoleName.ROLE_DEPTMANAGER)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
        Set<Role> roles = user.getRoles();
        roles.clear();
        roles.add(deptRole);
        user.setRoles(roles);
        userRepository.save(user);

        return this.dsl.update(EMPLOYEE)
                .set(EMPLOYEE.DEPARTMENT_ID, departmentId)
                .where(EMPLOYEE.ID.equal(employeeId))
                .execute();
    }

    /**
     * met à jour les informations de l'employé connecté
     * @param id: l'id de l'employé connecté
     * @param request: l'objet request
     * @param personalInfos: les informations de l'employé
     * @return TRUE si la mise à jour se passe bien
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Boolean updateEmployee(@PathVariable Integer id, HttpServletRequest request, @RequestBody PersonalInfos personalInfos) {
        EmployeeDto employeeDto = utilsService.getUserDto(request);
        if (Strings.hasText(personalInfos.getPassword())) {
            User user = userRepository.findByUsername(employeeDto.getUserName()).orElseThrow(() -> new RuntimeException("Compte introuvable"));
            user.setPassword(encoder.encode(personalInfos.getPassword()));
            userRepository.save(user);
        }
        if (Strings.hasText(personalInfos.getFirstName()) || Strings.hasText(personalInfos.getLastName())) {
            this.dsl.update(EMPLOYEE)
                    .set(EMPLOYEE.FIRST_NAME, personalInfos.getFirstName())
                    .set(EMPLOYEE.LAST_NAME, personalInfos.getLastName())
                    .where(EMPLOYEE.USER_NAME.eq(employeeDto.getUserName()))
                    .execute();
        }
        return Boolean.TRUE;
    }

    @GetMapping("/find/{name}")
    @PreAuthorize("hasRole('HRMANAGER') or hasRole('DEPTMANAGER') or hasRole('EMPLOYEE')")
    public Integer[] findEmployees(@PathVariable("name") String name) {
        return this.dsl
                .select(EMPLOYEE.ID)
                .from(EMPLOYEE)
                .where(EMPLOYEE.FIRST_NAME.contains(name).or(EMPLOYEE.FIRST_NAME.contains(name)))
                .fetchArray(EMPLOYEE.ID);
    }

    @GetMapping("/countEmployees")
    @PreAuthorize("hasRole('HRMANAGER')")
    public int count() {
        return this.dsl.fetchCount(EMPLOYEE);
    }

    /**
     * enrole un employé dans son département
     * @param request: l'objet request
     * @param username: l'employé à enroler
     * @return 1 si l'enrolement se passe bien
     */
    @PostMapping("/enrole")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public int enrole(HttpServletRequest request, @RequestBody String username) {
        EmployeeDto employeeDto = utilsService.getUserDto(request);
        if (!isUnassignedEmployee(username)) {
            throw new RuntimeException("Cet employé est déjà affecté à un département!");
        }
        return this.dsl.update(EMPLOYEE)
                .set(EMPLOYEE.DEPARTMENT_ID, employeeDto.getDepartmentId())
                .where(EMPLOYEE.USER_NAME.eq(username))
                .execute();
    }

    /**
     * vérifie si un employé est assigné à un département
     * @param username: le login de l'employé
     * @return TRUE si l'employé n'est affilié à aucun département et FALSE sinon
     */
    private boolean isUnassignedEmployee(String username) {
        Integer departmentId = getDepartmentId(username);
        return Objects.isNull(departmentId);
    }

    /**
     * permet de récupérer l'id du département auquel un employé est affilié
     * @param username: le login de l'employé
     * @return l'id du département
     */
    private Integer getDepartmentId(String username) {
        return this.dsl.select(EMPLOYEE.DEPARTMENT_ID).from(EMPLOYEE)
                .where(EMPLOYEE.USER_NAME.equal(username))
                .fetch().getValue(0, EMPLOYEE.DEPARTMENT_ID);
    }

    /**
     * sort un employé d'une direction
     * @param request: l'objet request
     * @param username: le login de l'employé
     * @return 1 si la mise à jour se passe bien
     */
    @PostMapping("/desenroler")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public int removeFromDept(HttpServletRequest request, @RequestBody String username) {
        EmployeeDto employeeDto = utilsService.getUserDto(request);
        if (isUnassignedEmployee(username)) {
            throw new RuntimeException("Cet employé n'est affecté à aucun département!");
        }
        if (!employeeDto.getDepartmentId().equals(getDepartmentId(username))) {
            throw new RuntimeException("Cet employé n'est pas affecté à votre département!");
        }
        return this.dsl.update(EMPLOYEE)
                .set(EMPLOYEE.DEPARTMENT_ID, (Integer) null)
                .where(EMPLOYEE.USER_NAME.eq(username))
                .execute();
    }

    /**
     * permet d'enregistrer le temps passé par projet
     * @param request: l'objet request
     * @param timesheetDto: les informations à logger
     * @return TRUE si l'enregistrement se passe bien
     */
    @PostMapping("/addTimeReport")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Boolean addTimeReport(HttpServletRequest request, @RequestBody TimesheetDto timesheetDto) {
        EmployeeDto employeeDto = utilsService.getUserDto(request);
        return timesheetService.reportTime(employeeDto, timesheetDto);
    }

    /**
     * Endpoint qui permet aux managers de département
     * de télécharger le rapport à une date donnée
     * @param date: la date du rapport
     * @param request: l'objet request qui permet de récupérer le département de l'utilisateur
     * @return le rapport s'il éxiste
     */
    @GetMapping("/getReport/{date}")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public ResponseEntity<Resource> getReport(@PathVariable String date, HttpServletRequest request) {
        EmployeeDto employeeDto = utilsService.getUserDto(request);
        String dept = this.dsl.select(DEPARTMENT.NO)
                .from(DEPARTMENT)
                .where(DEPARTMENT.ID.eq(employeeDto.getDepartmentId()))
                .fetchOneInto(String.class);
        Resource resource = utilsService.getReport(dept, LocalDate.parse(date));
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("text/csv"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
