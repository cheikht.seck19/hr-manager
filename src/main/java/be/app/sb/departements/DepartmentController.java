package be.app.sb.departements;

import be.app.sb.db.tables.daos.DepartmentDao;
import be.app.sb.db.tables.pojos.Department;
import be.app.sb.dto.DepartmentDto;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

import static be.app.sb.db.tables.Department.DEPARTMENT;

@RestController
@RequestMapping("/departments")
public class DepartmentController {
    private final DepartmentDao departmentDao;

    private final DSLContext dsl;

    /**
     * Constructeur
     * @param dsl
     * @param jooqConfiguration
     */
    public DepartmentController(DSLContext dsl, Configuration jooqConfiguration) {
        Settings settings = new Settings()
                .withRenderNameStyle(RenderNameStyle.AS_IS);
        jooqConfiguration.set(settings);
        this.departmentDao = new DepartmentDao(jooqConfiguration);
        this.dsl = dsl;
    }

    /**
     * liste tous les départements
     * @return la liste des départements
     */
    @GetMapping("/")
    @PreAuthorize("hasRole('HRMANAGER')")
    public List<Department> departments() {
        return this.departmentDao.findAll();
    }

    /**
     * récupère le département avec le code <b>no</b>
     * @param no: le code du département
     * @return le département avec le code <b>no</b> ou <b>null</b> s'il n'éxiste pas
     */
    @GetMapping("/{no}")
    @PreAuthorize("hasRole('HRMANAGER') or hasRole('DEPTMANAGER')")
    public Department findDepartment(@PathVariable("no") String no) {
        return this.dsl
                .selectFrom(DEPARTMENT)
                .where(DEPARTMENT.NO.eq(no))
                .fetchOne()
                .into(Department.class);
    }

    /**
     * ajoute un nouveau département
     * @param departmentDto: les informations du département
     * @return 1 si l'ajout se passe bien
     */
    @PostMapping("/")
    @PreAuthorize("hasRole('HRMANAGER')")
    public int add(@RequestBody DepartmentDto departmentDto) {
        return this.dsl
                .insertInto(DEPARTMENT)
                .set(DEPARTMENT.NO, departmentDto.getNo())
                .set(DEPARTMENT.NAME, departmentDto.getName())
                .execute();
    }

    /**
     * met à jour un département
     * @param no: le code du département
     * @param departmentDto: les informations mis à jour du département
     * @return 1 si la mise à jour se passe bien
     */
    @PutMapping("/{no}")
    @PreAuthorize("hasRole('HRMANAGER')")
    public int update(@PathVariable("no") String no,
                      @RequestBody DepartmentDto departmentDto) {
        return this.dsl
                .update(DEPARTMENT)
                .set(DEPARTMENT.NAME, departmentDto.getName())
                .where(DEPARTMENT.NO.equal(no))
                .execute();
    }

    /**
     * supprime un département
     * @param no: le code du département
     * @return 1 si la suppression se passe bien
     */
    @DeleteMapping("/{no}")
    @PreAuthorize("hasRole('HRMANAGER')")
    public int delete(@PathVariable("no") @NotBlank String no) {
        return this.dsl
                .delete(DEPARTMENT)
                .where(DEPARTMENT.NO.equal(no))
                .execute();
    }
}
