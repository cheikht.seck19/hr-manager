package be.app.sb.repositories;

import be.app.sb.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    List<User> findAllByEmployeeIn(List<Integer> ids);
    List<User> findAllByEmployeeIsNotNull();
}
