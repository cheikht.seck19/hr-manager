package be.app.sb.dto;

import be.app.sb.model.Project;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProjectDto {
    private Long id;
    private String name;

    public ProjectDto(Project project) {
        this.id = project.getId();
        this.name = project.getName();
    }
}
