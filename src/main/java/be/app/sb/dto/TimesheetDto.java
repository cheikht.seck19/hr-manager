package be.app.sb.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TimesheetDto {
    private Long projectId;

    private LocalDateTime startedAt;

    private LocalDateTime finishedAt;
}
