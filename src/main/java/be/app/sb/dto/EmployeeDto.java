package be.app.sb.dto;

import be.app.sb.db.tables.pojos.Employee;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmployeeDto extends Employee {
    private Long userId;
    @NotBlank(message = "Le mot de passe ne doit pas être vide")
    private String password;
    private List<String> role;

    public EmployeeDto(Integer id, String userName) {
        this.setId(id);
        this.setUserName(userName);
    }
}
