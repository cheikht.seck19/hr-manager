package be.app.sb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@Getter
@Setter
public class DepartmentDto {

    @NotBlank
    private String no;

    @NotBlank
    private String name;

}
