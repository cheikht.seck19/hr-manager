package be.app.sb.projects;

import be.app.sb.dto.ProjectDto;
import be.app.sb.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public List<ProjectDto> index() {
        return projectService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public ProjectDto get(@PathVariable("id") Long id) {
        return projectService.findById(id);
    }

    @PostMapping("/")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public ProjectDto save(@RequestBody String name) {
        return projectService.save(name);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public ProjectDto update(@PathVariable("id") Long id, @RequestBody String name) {
        return projectService.update(id, name);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('DEPTMANAGER')")
    public Boolean delete(@PathVariable("id") Long id) {
        return projectService.delete(id);
    }

}
