package db.migration;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.jooq.DSLContext;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;
import java.util.List;

import static org.jooq.impl.DSL.*;

public class V0004__Add_departments extends BaseJavaMigration {

    @Override
    public void migrate(Context context) throws Exception {
        ClassPathResource cpr = new ClassPathResource("employees_departments.csv");
        InputStream is_ = cpr.getInputStream();
        try (InputStream is = cpr.getInputStream()) {
            CsvParserSettings settings = new CsvParserSettings();
            settings.getFormat().setDelimiter(',');
            settings.getFormat().setLineSeparator("\n");
            CsvParser parser = new CsvParser(settings);
            List<String[]> rows = parser.parseAll(is);
            try (DSLContext dsl = using(context.getConnection())) {
                for (String[] row : rows) {
                    dsl.update(table("employee"))
                            .set(field("department_id", Integer.class),
                                    select(field("id", Integer.class))
                                            .from(table("department"))
                                            .where(field("no", String.class).eq(row[1])).limit(1).asField())
                            .where(field("user_name", String.class).eq(row[0])).execute();
                }

                int employeesWithUnsetDep = dsl.selectCount()
                        .from(table("employee"))
                        .innerJoin(table("department"))
                        .on(field("department.id", Integer.class)
                                .equal(field("employee.department_id", Integer.class)))
                        .where(field("department.no", String.class).eq("dddd"))
                        .fetchOne(0, Integer.class);

                if (employeesWithUnsetDep == 0) {
                    dsl.delete(table("department")).where(field("no").eq("dddd")).execute();
                }
            }
        }
    }

}
